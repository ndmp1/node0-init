#!/bin/bash

# Update and upgrade system packages
update_system() {
    echo "Updating package list..."
    sudo apt-get update
    
    echo "Upgrading installed packages..."
    sudo apt-get upgrade -y
}

# Install a specific software package
install_software() {
    local software_name=$1
    echo "Installing ${software_name}..."
    sudo apt-get install "${software_name}" -y
}

# Create a directory with full permissions
create_directory_with_permissions() {
    local directory_path=$1
    local permissions=$2
    echo "Creating directory '${directory_path}' with permissions '${permissions}'..."
    mkdir -p "${directory_path}"
    sudo chmod "${permissions}" "${directory_path}"
}

# Create a Docker configuration file
create_docker_config_file() {
    local config_path=$1
    local data_root=$2
    echo "Creating Docker configuration file '${config_path}'..."
    echo "{\"data-root\":\"${data_root}\"}" | sudo tee "${config_path}" > /dev/null
}

# Run a bash command and display its output
run_bash_command() {
    local command=$1
    echo "Running command: ${command}"
    eval "${command}"
}

main() {
    update_system
    install_software "docker.io"
    create_directory_with_permissions "/home/pentest/docker_storage" "777"
    create_docker_config_file "/etc/docker/daemon.json" "/home/pentest/docker_storage/"
    echo "adding csoc user to docker group and restarting docker"
    run_bash_command "sudo usermod -aG docker csoc"
    run_bash_command "sudo systemctl restart docker"
    # Pause for 15 seconds before running the final command
    echo "Pausing for 15 seconds...to allow docker service to restart!"
    sleep 15    
    run_bash_command "curl https://downloads.horizon3ai.com/utilities/checkenv.sh | bash"
}

# Execute the main function
main