import subprocess
import os
import json

def run_command(command):
    """
    Run a shell command and return the output
    """
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()

    if process.returncode != 0:
        print(f"Error executing command '{command}': {stderr}")
    else:
        print(f"Command '{command}' executed successfully: {stdout}")

def update_system():
    """
    Update and upgrade system packages
    """
    print("Updating package list...")
    run_command("sudo apt-get update")
    
    print("Upgrading installed packages...")
    run_command("sudo apt-get upgrade -y")

def install_software(software_name):
    """
    Install specific software package
    """
    print(f"Installing {software_name}...")
    run_command(f"sudo apt-get install {software_name} -y")

def create_directory_with_permissions(directory_path, permissions):
    """
    Create a new directory and set its permissions
    """
    try:
        # Create the directory
        os.makedirs(directory_path, exist_ok=True)
        print(f"Directory '{directory_path}' created successfully.")
        
        # Set the directory's permissions
        run_command(f"sudo chmod {permissions} {directory_path}")
        print(f"Permissions '{permissions}' set for directory '{directory_path}'.")
    except OSError as error:
        print(f"Error creating directory '{directory_path}': {error}")

def create_docker_config_file(config_path, data):
    """
    Create a new Docker configuration file with the given data
    """
    try:
        # Ensure the directory exists before creating the file
        os.makedirs(os.path.dirname(config_path), exist_ok=True)
        
        # Write the JSON data to the config file
        with open(config_path, 'w') as config_file:
            json.dump(data, config_file, indent=4)
        
        print(f"Configuration file '{config_path}' created successfully.")
    except OSError as error:
        print(f"Error creating Docker configuration file '{config_path}': {error}")

def run_bash_command(command):
    """
    Run a bash command and display its output
    """
    try:
        output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
        print("Command output:\n", output.decode())
    except subprocess.CalledProcessError as e:
        print("Error executing command:\n", e.output.decode())

if __name__ == "__main__":
    #update_system()
    install_software("docker.io")
    create_directory_with_permissions("/home/csoc/docker_storage", "777")
    docker_config_data = {
        "data-root": "/home/csoc/docker_storage/"
    }
    docker_config_path = "/etc/docker/daemon.json"
    create_docker_config_file(docker_config_path, docker_config_data)
    bash_command = "curl https://downloads.horizon3ai.com/utilities/checkenv.sh | bash"
    run_bash_command(bash_command)
